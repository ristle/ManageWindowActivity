### Manage Used Programs in Linux

This project provides simple activity managing using linux tools

- [Install](#install)
- [Run](#run)
    - [debug](#debug)
    - [release](#release)
- [TO DO](#todo)

## Install

For install needed packages you need to run those commands

    python3 -m venv .windows-env
    source .windows-env/bin/activate
    pip install -r requirements.txt
   
Before running you should get your telegram bot api key
them paste it in **data/keys.json** like it provide in **keys.json.env**

    {
      "telegram": "your telegram key"
    }

## Run 

This is provides simple output information about only current window

    source .windows-env/bin/activate
    python main.py
    
#### Debug

in windows/CheckWindow.py

Pass argument to __CheckWindow__:

    @logger.catch
    def test_run_activity():
        ck = CheckWindow(True)

#### Release
   
in windows/CheckWindow.py by default is release mode

Do not pass any argument to __CheckWindow__:

    @logger.catch
    def test_run_activity():
        ck = CheckWindow()
        
## TODO

- [X] Add telegram output info
- [X] Run in background
- [X] Add chartInfo
- [X] Log to server in Json format
- [X] Make nice json data about application
- [X] Manage time for each program ( sum running time )

- [ ] Add google phone information
- [ ] Add task management

  