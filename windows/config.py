#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
from loguru import logger as log

NAME = None
TOKEN = None
LAST_REPLY_MESSAGE = None
TIMEZONE = 'Europe/Moscow'
TIMEZONE_COMMON_NAME = 'Moscow'
trust_list = ['ristleell']


@log.catch
def createOutputArrays(windows: dict, names: list):
    size = []
    other_size = 0
    for key, value in windows.items():
        if key == 'current_window':
            continue
        size.append(value["delta"])
    size.sort()
    size.reverse()

    output_array = list()
    output_names = list()
    else_array = list()
    else_names = list()
    for size_, name in zip(size, names):
        if (size_ / size[0]) < 0.05:
            other_size += size_
            else_array.append(size_)
            else_names.append(name)
        else:
            output_array.append(size_)
            output_names.append(name)

    output_array.append(other_size)
    output_names.append("Else")

    return output_array, output_names, (else_array, else_names)


@log.catch
def get_legend(windows: dict, name: str):
    sec = windows[name]['end'] - windows[name]['start']
    min_ = sec / 60.
    hours = min_ / 60.
    return "{:.1}".format(str(hours)) + ":{:.1}".format(str(min_)) + ":{:.3}".format(str(sec))


@log.catch
def load_telegram_token() -> str:
    with open('./data/keys.json', encoding='utf-8') as fh:
        data = json.load(fh)
    if data["telegram"] is None:
        raise FileNotFoundError
    return data["telegram"]


@log.catch
def load_activity_list() -> dict:
    with open('./data/activity.json', encoding='utf-8') as fh:
        data = json.load(fh)
    return data or dict()


@log.catch
def save_activity_list(js):
    with open("./data/activity.json", 'w', encoding='utf8') as json_file:
        json.dump(js, json_file, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': '))


@log.catch
def simple_legend(text: str, names: list, array: list, windows: dict) -> str:
    for key, value in zip(names, array):
        if key == 'current_window' or key == 'Else':
            continue

        text += "*" + key + "*: " + "_" + get_legend(windows, key) + "_\t\n"
    return text


@log.catch
def prepare_text() -> str:
    windows = load_activity_list()
    names = [key for key, value in windows.items()]
    output_array, output_names, else_ = createOutputArrays(windows, names)
    print(else_[1])

    output_text = simple_legend(str(), output_names, output_array, windows)
    output_text += "\n * Else section: *\n"
    output_text = simple_legend(output_text, else_[1], else_[0], windows)

    return output_text
