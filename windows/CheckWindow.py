#!/usr/bin/env python3

import re
import os
import datetime
from loguru import logger
from time import sleep, time
import windows.config as config
from subprocess import PIPE, Popen
from matplotlib import pyplot as plt
from windows.config import get_legend
from threading import Thread, Event, Lock
from palettable.colorbrewer.qualitative import Pastel1_7


class CheckWindow:
    def __init__(self, debug=False):
        import matplotlib
        matplotlib.use('Agg')

        self.debug = debug
        self.windows = config.load_activity_list()  # needs to store all data of used windows
        self.data_lock = Lock()  # for shared memory access

        self.activity_thread = Thread(target=self.runActivity, args=())
        self.activity_thread.start()

    @logger.catch
    def visualize(self):
        with self.data_lock:
            current_window = self.windows['current_window']
            del self.windows['current_window']

            names = [key for key, value in self.windows.items()]
            output_array, output_names, _ = config.createOutputArrays(self.windows, names)

            # Create a circle for the center of the plot
            my_circle = plt.Circle((0, 0), 0.7, color='white')

            plt.pie(output_array, labels=output_names, colors=Pastel1_7.hex_colors)
            # plt.legend([get_legend(self.windows, name) for name, value in
            #             self.windows.items()])
            p = plt.gcf()
            p.gca().add_artist(my_circle)

            if os.path.exists('./data/latest.png'):
                today = datetime.date.today()

                yesterday = today - datetime.timedelta(days=1)
                os.rename("./data/latest.png", "./data/" + yesterday.strftime("%b %d %Y") + ".png")

            plt.savefig('./data/latest.png')
            plt.close()

            self.windows['current_window'] = current_window

    @staticmethod
    @logger.catch()
    def add2dict(windows: dict, window_name: str, program_name: str, *all):
        """
        This function simple add additional information about our windows
        :param windows: doct data about everything
        :param window_name: current window name
        :param program_name: current programm name
        :param all: everythin else
        :return: see windows
        """
        if 'current_window' in list(windows.keys()):
            if windows['current_window'] == program_name:
                windows[program_name]['end'] = time()
                windows[program_name]['delta'] += windows[program_name]['end'] - windows[program_name]['start']

                config.save_activity_list(windows)
                return windows
        if program_name in list(windows.keys()):
            windows[program_name]['start'] = time()
            windows[program_name]['window name'] = window_name

            config.save_activity_list(windows)
            return

        windows[program_name] = {
            'start': time(),
            'end': time(),
            'window name': window_name,
            'delta': 0.5
        }
        windows['current_window'] = program_name

        config.save_activity_list(windows)
        return windows

    @staticmethod
    @logger.catch
    def getActivityName(windows: dict, data_lock: Lock):
        """
        This function get current window name and program name
        :return: { 'window name': current_window_name,
                   'program name': current_program_name }
        """
        root = Popen(['xprop', '-root', '_NET_ACTIVE_WINDOW'], stdout=PIPE)
        stdout, stderr = root.communicate()
        m = re.search(b'^_NET_ACTIVE_WINDOW.* ([\w]+)$', stdout)

        if m is not None:
            window_id = m.group(1)

            window_name = None
            window = Popen(['xprop', '-id', window_id, 'WM_NAME'], stdout=PIPE)
            stdout, stderr = window.communicate()
            window_match = re.match(b'WM_NAME\(\w+\) = (?P<name>.+)$', stdout)
            if window_match is not None:
                window_name = window_match.group('name').decode('UTF-8').strip('"')

            program_name = None
            process = Popen(['xprop', '-id', window_id, 'WM_CLASS'], stdout=PIPE)
            stdout, stderr = process.communicate()
            process_match = re.match(b'WM_CLASS\(\w+\) = (?P<name>.+)$', stdout)
            if process_match is not None:
                _, program_name = process_match.group('name').decode('UTF-8').split(', ')
                program_name = program_name.strip('"')

            with data_lock:
                if program_name:
                    tewindows = CheckWindow.add2dict(windows, window_name, program_name)

            return {
                'window name': window_name,
                'program name': program_name,
                'start': time(),
                'end': time(),
                'delta': 0.5
            }
        return {
            'window name': "Else",
            'program name': "Else",
            'start': time(),
            'end': time(),
            'delta': 0.5
        }

    @logger.catch
    def runActivity(self):
        """
        Function which runs in thread and used for testing purpose
        :return: nothing
        """
        while True:
            try:
                debug_info = CheckWindow.getActivityName(self.windows, self.data_lock)
                sleep(0.5)
                if self.debug:
                    logger.debug(debug_info)
            except KeyboardInterrupt:
                print('Interrupted')
                config.save_activity_list(self.windows)
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)


@logger.catch
def test_output():
    """
    Simple test output of getting current window and program names
    :return: nothing
    """
    sleep(1.5)

    a = CheckWindow.getActivityName({}, Lock())

    print('''
        'window name':   %s,
        'program name': %s
        ''' % (a['window name'], a['program name']))


@logger.catch
def test_run_activity():
    ck = CheckWindow(False)

    sleep(6)
    ck.visualize()


if __name__ == '__main__':
    test_run_activity()
