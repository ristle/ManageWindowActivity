#!/usr/bin/env python3
from windows.CheckWindow import CheckWindow
from windows.CheckWindow import test_output
from windows.telegramBot import bot
from loguru import logger as log
import windows.config


def run():
    log.info("Starting telegram bot")

    bot.polling(none_stop=True)
