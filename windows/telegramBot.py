#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import telebot
import telepot
from loguru import logger
import windows.config as config
from windows.CheckWindow import CheckWindow

bot = telebot.TeleBot(config.load_telegram_token())
ck = CheckWindow(False)

@bot.message_handler(commands=['start'])
def start_command(message):
    bot.send_message(
        message.chat.id,
        'Шалом, я предназначен для того, чтобы смотреть сколько ты, сука, на компе сидел в каком приложении\n' +
        'Если есть вопросы, то напишите /help.'
    )

# for stupid
@bot.message_handler(commands=['help'])
def help_command(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(
        telebot.types.InlineKeyboardButton(
            'message to developer', url='t.me/ristleell'
        )
    )
    keyboard.add(
        telebot.types.InlineKeyboardButton(
            'GitLab', url='https://gitlab.com/ristle/ManageWindowActivity.git'
        )
    )
    bot.send_message(
        message.chat.id,
        'Бот работает корректно только на Linux и с этим надо смириться. Он шлет количество часов, минут и секунд за каждым приложени \n' + \
        'Можно дальше расширить функционал, но мне пока норм',
        reply_markup=keyboard
    )


# needed for one line adding new beer
@bot.message_handler(commands=['get'])
def exchange_command(message):
    config.NAME = None
    if message.from_user.username not in config.trust_list:
        bot.send_message(message.chat.id, "У Вас нет прав на получение информации")
        return
    try:
        ck.visualize()
    except:
        logger.error("FUCK IT! cannot create image")
        bot.send_message("Дерьмо! не могу создать визуализацю")
        return

    img = open('./data/latest.png', 'rb')
    text = config.prepare_text()
    bot.send_photo(message.chat.id, img, text, reply_to_message_id=message.message_id, parse_mode='Markdown')

    img.close()
